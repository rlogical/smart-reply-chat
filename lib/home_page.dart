import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_app/registration_page.dart';
import 'package:flutter_chat_app/utils/app_utils.dart';
import 'package:flutter_chat_app/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'chat/message_page.dart';

class HomePage extends StatefulWidget {
  final SharedPreferences prefs;

  HomePage({this.prefs});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final db = FirebaseFirestore.instance;
  CollectionReference contactsReference;
  DocumentReference profileReference;
  DocumentSnapshot profileSnapshot;

  final _yourNameController = TextEditingController();
  bool editName = false;
  CollectionReference chatReference;

  @override
  void initState() {
    super.initState();
    contactsReference = db.collection("users");
    profileReference =
        db.collection("users").doc(widget.prefs.getString('uid'));
    chatReference = db.collection("chats");

    profileReference.snapshots().listen((querySnapshot) {
      profileSnapshot = querySnapshot;
      widget.prefs.setString('name', profileSnapshot.get("name"));
      widget.prefs
          .setString('profile_photo', profileSnapshot.get("profile_photo"));

      setState(() {
        _yourNameController.text = profileSnapshot.get("name");
      });
    });
  }

  generateContactTab() {
    return Column(
      children: <Widget>[
        StreamBuilder<QuerySnapshot>(
          stream: contactsReference.snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) return new Text("No Contacts");
            return Expanded(
              child: new ListView(
                children: generateContactList(snapshot),
              ),
            );
          },
        )
      ],
    );
  }

  getLastMessage(String mChatId) {
    CollectionReference lastMessageRef =
        chatReference.doc(mChatId).collection('messages');
    lastMessageRef
        .orderBy('time', descending: true)
        .limit(1)
        .snapshots()
        .listen((querySnapshot) {
      if (querySnapshot.docs.isNotEmpty)
        return querySnapshot.docs.first.get("text");
    });
  }

  generateContactList(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs
        .map<Widget>(
          (doc) => widget.prefs.getString('uid') != doc.id
              ? InkWell(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    child: ListTile(
                      leading: CircleAvatar(
                        child: Image.network(doc["profile_photo"] != ""
                            ? doc["profile_photo"]
                            : "https://avatarfiles.alphacoders.com/791/79102.png"),
                      ),
                      title: Text(doc["name"]),
                      subtitle: getLastMessage(doc.id),
                      trailing: Icon(Icons.chevron_right),
                    ),
                  ),
                  onTap: () async {
                    await db
                        .collection('chats')
                        .where('contact1',
                            isEqualTo: widget.prefs.getString('uid'))
                        .where('contact2', isEqualTo: doc.id)
                        .get()
                        .then((value) async {
                      List<DocumentSnapshot> documents = value.docs;
                      if (documents.length == 0) {
                        await db
                            .collection('chats')
                            .where('contact2',
                                isEqualTo: widget.prefs.getString('uid'))
                            .where('contact1', isEqualTo: doc.id)
                            .get()
                            .then((value2) async {
                          documents = value2.docs;
                          if (documents.length == 0) {
                            await db.collection('chats').add({
                              'contact1': widget.prefs.getString('uid'),
                              'contact2': doc.id
                            }).then((documentReference) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => ChatPage(
                                    prefs: widget.prefs,
                                    chatId: documentReference.id,
                                    title: doc["name"],
                                    profilePic: doc["profile_photo"],
                                  ),
                                ),
                              );
                            }).catchError((e) {});
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ChatPage(
                                  prefs: widget.prefs,
                                  chatId: documents[0].id,
                                  title: doc["name"],
                                  profilePic: doc["profile_photo"],
                                ),
                              ),
                            );
                          }
                        });
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ChatPage(
                              prefs: widget.prefs,
                              chatId: documents[0].id,
                              title: doc["name"],
                              profilePic: doc["profile_photo"],
                            ),
                          ),
                        );
                      }
                      ;
                    });
                  },
                )
              : Container(),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    changeStatusBar(Brightness.light);
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: dark_blue,
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 40, bottom: 20),
              color: dark_blue,
              child: Row(
                children: [
                  Expanded(
                      child: Text(
                    "Messages",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )),
                  GestureDetector(
                    onTap: () {
                      changeStatusBar(Brightness.dark);
                      FirebaseAuth.instance.signOut().then((response) {
                        widget.prefs.remove('is_verified');
                        widget.prefs.remove('mobile_number');
                        widget.prefs.remove('uid');
                        widget.prefs.remove('name');
                        widget.prefs.remove('profile_photo');
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) =>
                            RegistrationPage(prefs: widget.prefs,),
                          ),
                        );
                      });
                    },
                    child: Icon(
                      Icons.logout,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  )
                ],
              ),
            ),
            Expanded(
                child: new Container(
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(40.0),
                    topRight: const Radius.circular(40.0),
                  )),
              child: new Center(
                child: generateContactTab(),
              ),
            ))
          ],
        ),
      ),
    );
  }
}
