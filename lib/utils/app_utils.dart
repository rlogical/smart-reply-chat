
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

changeStatusBar(Brightness mIconColor){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: mIconColor
  ));
}