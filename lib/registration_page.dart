import 'dart:convert';

import 'package:awesome_loader/awesome_loader.dart';
import 'package:bottom_loader/bottom_loader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_app/utils/colors.dart';
import 'package:http/http.dart' as http;
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_page.dart';

class RegistrationPage extends StatefulWidget {
  final SharedPreferences prefs;

    RegistrationPage({this.prefs});

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  String phoneNo = "";
  String smsOTP;
  String verificationId;
  String errorMessage = '';
  FirebaseAuth _auth = FirebaseAuth.instance;
  final db = FirebaseFirestore.instance;
  TextEditingController nameController = new TextEditingController();
  bool isProgress = false;
  BottomLoader bl;

  Future<void> verifyPhone() async {
    final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
      bl.close();
      smsOTPDialog(context).then((value) {});
    };
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: "+91" + this.phoneNo,
          // PHONE NUMBER TO SEND OTP
          codeAutoRetrievalTimeout: (String verId) {
            //Starts the phone number verification process for the given phone number.
            //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
            this.verificationId = verId;
          },
          codeSent: smsOTPSent,
          // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
          timeout: const Duration(seconds: 20),
          verificationCompleted: (AuthCredential phoneAuthCredential) {
            print(phoneAuthCredential);
          },
          verificationFailed: (FirebaseAuthException e) {
            print('${e.message}');
          });
    } catch (e) {
      handleError(e);
    }
  }

  Future<bool> smsOTPDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
              backgroundColor: Colors.transparent,
              insetPadding: EdgeInsets.all(10),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    height: 200,
                    child: Column(
                      children: [
                        Container(
                          child: Text("Enter sms Code",
                              style: TextStyle(fontSize: 20),
                              textAlign: TextAlign.center),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        PinEntryTextField(
                          showFieldAsBox: true,
                          fields: 6,
                          onSubmit: (String pin) {
                            this.smsOTP = pin;
                          }, // end onSubmit
                        ), // end PinEntryTextField()
                        Container(
                          alignment: Alignment.bottomRight,
                          child: TextButton(
                            child: Text('Done'),
                            onPressed: () {
                              signIn();
                            },
                          ),
                        )
                      ],
                    ), // end Padding()
                  ),
                ],
              ));
        });
  }

  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      final User user = (await _auth.signInWithCredential(credential)).user;
      showSnackbar("Successfully Signed In");
      final User currentUser = await _auth.currentUser;
      assert(user.uid == currentUser.uid);
      Navigator.of(context).pop();
      CollectionReference mobileRef = db.collection("mobiles");

      await mobileRef.get().then((value) async {
        if (value.docs.isNotEmpty) {
          print(" not empty");
          await mobileRef
              .doc(phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''))
              .get()
              .then((mobileDoc) async {
            if (!mobileDoc.exists) {
              await db.collection("users").add({
                'name': nameController.text,
                'mobile': phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''),
                'profile_photo': "",
              }).then((documentReference) {
                widget.prefs.setBool('is_verified', true);
                widget.prefs.setString(
                  'mobile',
                  phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''),
                );
                widget.prefs.setString('uid', documentReference.id);
                widget.prefs.setString('name', nameController.text);
                widget.prefs.setString('profile_photo', "");

                mobileRef
                    .doc(phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''))
                    .set({'uid': documentReference.id}).then(
                        (documentReference) async {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => HomePage(prefs: widget.prefs)));
                }).catchError((e) {
                  print(e);
                });
              }).catchError((e) {
                print(e);
              });
            } else {
              CollectionReference userCollection = db.collection("users");
              await userCollection
                  .doc(mobileDoc.get("uid"))
                  .get()
                  .then((mobileuser) async {
                widget.prefs.setBool('is_verified', true);
                widget.prefs.setString(
                  'mobile_number',
                  phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''),
                );
                widget.prefs.setString('uid', mobileuser.id);
                widget.prefs.setString('name', nameController.text);
                widget.prefs.setString(
                    'profile_photo', mobileuser.get("profile_photo"));

                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => HomePage(prefs: widget.prefs),
                  ),
                );
              });
            }
          });
        } else {
          print("empty");
          await db.collection("users").add({
            'name': nameController.text,
            'mobile': phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''),
            'profile_photo': "",
          }).then((documentReference) {
            widget.prefs.setBool('is_verified', true);
            widget.prefs.setString(
              'mobile',
              phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), ''),
            );
            widget.prefs.setString('uid', documentReference.id);
            widget.prefs.setString('name', nameController.text);
            widget.prefs.setString('profile_photo', "");

            mobileRef.doc(phoneNo.replaceAll(new RegExp(r'[^\w\s]+'), '')).set(
                {'uid': documentReference.id}).then((documentReference) async {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => HomePage(prefs: widget.prefs)));
            }).catchError((e) {
              print(e);
            });
          }).catchError((e) {
            print(e);
          });
        }
      });
    } catch (e) {
      handleError(e);
    }
  }

  handleError(PlatformException error) {
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMessage = 'Invalid Code';
          showSnackbar(errorMessage);
        });
        Navigator.of(context).pop();
        smsOTPDialog(context).then((value) {});
        break;
      default:
        setState(() {
          errorMessage = error.message;
          showSnackbar(errorMessage);
        });

        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 15, top: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        child: Image.asset(
                          "ic_mobile.png",
                          height: 120,
                          width: 120,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 50, left: 20, right: 20),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: dark_blue,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40),
                          topLeft: Radius.circular(40))),
                  child: Column(
                    children: [
                      Flexible(
                          child: TextField(
                        controller: nameController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Your Name",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                      )),
                      const SizedBox(
                        height: 30,
                      ),
                      Flexible(
                          child: TextField(
                        //controller: _phoneNumberController,
                        onChanged: (value) {
                          this.phoneNo = value;
                        },
                        keyboardType: TextInputType.number,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Your phone number",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                      )),
                      const SizedBox(
                        height: 60,
                      ),
                      Flexible(
                          child: GestureDetector(
                        onTap: () async {
                          if (nameController.text == "") {
                            showSnackbar("Please enter your Name.");
                            //showSnackbar(EmailFieldValidator.validate(nameController.text));
                          } else if (this.phoneNo == '') {
                            showSnackbar("Please enter your phone Number.");
                          } else {
                            bl = new BottomLoader(context,
                                showLogs: false,
                                isDismissible: true,
                                loader: AwesomeLoader(
                                  loaderType: AwesomeLoader.AwesomeLoader2,
                                  color: dark_blue,
                                ));
                            bl.style(
                                message: 'Please wait...',
                                messageTextStyle:
                                    TextStyle(color: dark_blue, fontSize: 12));
                            bl.display();
                            verifyPhone();
                          }
                        },
                        child: Container(
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color(0xffF1F5F9),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            "Submit",
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
                //    )
              )
            ],
          ),
        ],
      ),
    );
  }

  void showSnackbar(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }
}
