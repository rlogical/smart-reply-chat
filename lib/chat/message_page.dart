import 'package:awesome_loader/awesome_loader.dart';
import 'package:bottom_loader/bottom_loader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_app/utils/app_utils.dart';
import 'package:flutter_chat_app/utils/colors.dart';
import 'package:flutter_smart_reply/flutter_smart_reply.dart';

import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'gallary_page.dart';

class ChatPage extends StatefulWidget {
  final SharedPreferences prefs;
  final String chatId;
  final String title;
  final String profilePic;

  ChatPage({this.prefs, this.chatId, this.title, this.profilePic});

  @override
  ChatPageState createState() {
    return new ChatPageState();
  }
}

class ChatPageState extends State<ChatPage> {
  final db = FirebaseFirestore.instance;
  CollectionReference chatReference;
  final TextEditingController _textController = new TextEditingController();
  bool _isWritting = false;

  List<TextMessage> _textMessages = [];

  List<String> _replies = List.empty();

  //bool isSelfMode = true;
  BottomLoader bl;

  Future<void> updateSmartReplies() async {
    try {
      _replies = await FlutterSmartReply.getSmartReplies(_textMessages);
    } on PlatformException {}
  }

  @override
  void initState() {
    super.initState();
    chatReference =
        db.collection("chats").doc(widget.chatId).collection('messages');
    getLastMessage();
  }

  List<Widget> generateSenderLayout(DocumentSnapshot documentSnapshot) {
    return <Widget>[
      new Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          new Container(
            padding: EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
            margin: EdgeInsets.only(right: 20),
            decoration: BoxDecoration(
              color: dark_blue,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20)),
            ),
            child: documentSnapshot.get('image_url') != ''
                ? InkWell(
                    child: new Container(
                      child: Image.network(
                        documentSnapshot.get('image_url'),
                        fit: BoxFit.fitWidth,
                      ),
                      height: 150,
                      width: 150.0,
                      color: Color.fromRGBO(0, 0, 0, 0.2),
                      padding: EdgeInsets.all(5),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => GalleryPage(
                            imagePath: documentSnapshot.get("image_url"),
                          ),
                        ),
                      );
                    },
                  )
                : Text(
                    documentSnapshot.get('text'),
                    style: TextStyle(color: Colors.white),
                  ),
          ),
          Container(
            padding: EdgeInsets.only(right: 20, top: 2),
            child: Text(
              documentSnapshot.get('time') != null
                  ? getTimeFormat(documentSnapshot.get('time'))
                  : "",
              style: TextStyle(fontSize: 12, color: Colors.grey),
            ),
          ),
        ],
      ))
    ];
  }

  getTimeFormat(Timestamp mTime) {
    DateTime mDate = DateTime.parse(mTime.toDate().toString());
    String timeStr = DateFormat.jm().format(mDate);
    return timeStr;
  }

  List<Widget> generateReceiverLayout(DocumentSnapshot documentSnapshot) {
    return <Widget>[
      new Expanded(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              padding:
                  EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
              margin: const EdgeInsets.only(top: 5.0, left: 20),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
              ),
              child: documentSnapshot.get('image_url') != ''
                  ? InkWell(
                      child: new Container(
                        child: Image.network(
                          documentSnapshot.get('image_url'),
                          fit: BoxFit.fitWidth,
                        ),
                        height: 150,
                        width: 150.0,
                        //color: Color.fromRGBO(0, 0, 0, 0.2),
                        padding: EdgeInsets.all(5),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => GalleryPage(
                              imagePath: documentSnapshot.get("image_url"),
                            ),
                          ),
                        );
                      },
                    )
                  : Text(
                      documentSnapshot.get('text'),
                      style: TextStyle(color: dark_blue),
                    ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, top: 2),
              child: new Text(
                documentSnapshot.get('time') != null
                    ? getTimeFormat(documentSnapshot.get('time'))
                    : "",
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  generateMessages(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs
        .map<Widget>((doc) => Container(
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: new Row(
                children: doc.get('sender_id') != widget.prefs.getString('uid')
                    ? generateReceiverLayout(doc)
                    : generateSenderLayout(doc),
              ),
            ))
        .toList();
  }

  Widget _buildSmartReplyRow() {
    return Container(
      height: 30,
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment:
        MainAxisAlignment.end,
        children: [
          _buildSmartReplyChips(),
          Icon(Icons.home),
        ],
      ),
    );
  }

  Widget _buildSmartReplyChips() => ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: _replies.map(_buildSmartReplyChip).toList(),
      );

  Widget _buildSmartReplyChip(String text) {
    return ActionChip(
      label: Text(text),
      onPressed: () => _sendText(text),
    );
  }

  getLastMessage() {
    chatReference
        .orderBy('time', descending: true)
        .limit(1)
        .snapshots()
        .listen((querySnapshot) {
      if (querySnapshot.docs.isNotEmpty &&
          querySnapshot.docs.first.get('sender_id') !=
              widget.prefs.getString('uid')) {
        _addMessage(querySnapshot.docs.first.get("text"));
        print(querySnapshot.docs.first.get("text"));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    changeStatusBar(Brightness.dark);
    return WillPopScope(
        child: Scaffold(
          backgroundColor: dark_blue,
          body: new Column(
              children: <Widget>[
                Expanded(
                    child: new Container(
                      padding: EdgeInsets.only(top: 20),
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.only(
                              bottomLeft: const Radius.circular(40.0),
                              bottomRight: const Radius.circular(40.0),
                            )),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: Row(
                                children: [
                                  IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios,
                                      color: dark_blue,
                                    ),
                                    onPressed: () {
                                      changeStatusBar(Brightness.light);
                                      Navigator.pop(context);
                                    },
                                  ),
                                  Expanded(
                                    child: Text(
                                      widget.title,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: dark_blue,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  new Container(
                                      margin: const EdgeInsets.only(
                                          right: 8.0, left: 20),
                                      child: new CircleAvatar(
                                        backgroundImage: new NetworkImage(widget
                                                    .profilePic !=
                                                ""
                                            ? widget.profilePic
                                            : "https://avatarfiles.alphacoders.com/791/79102.png"),
                                      ))
                                ],
                              ),
                            ),
                            StreamBuilder<QuerySnapshot>(
                              stream: chatReference
                                  .orderBy('time', descending: true)
                                  .snapshots(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<QuerySnapshot> snapshot) {
                                if (!snapshot.hasData)
                                  return new Text("No Chat");
                                return Expanded(
                                  child: new ListView(
                                    reverse: true,
                                    children: generateMessages(snapshot),
                                  ),
                                );
                              },
                            ),
                            _buildSmartReplyRow(),
                            new Divider(height: 1.0),
                          ],
                        ))),
                new Container(
                  color: dark_blue,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: _buildTextComposer(),
                ),
                new Builder(builder: (BuildContext context) {
                  return new Container(width: 0.0, height: 0.0);
                })
              ],
            ),
        ),
        onWillPop: () async {
          changeStatusBar(Brightness.light);
          return true;
        });
  }

  IconButton getDefaultSendButton() {
    return new IconButton(
      icon: new Icon(Icons.send, color: Colors.white),
      onPressed: _isWritting ? () => _sendText(_textController.text) : null,
    );
  }

  Widget _buildTextComposer() {
    return new IconTheme(
        data: new IconThemeData(
          color: _isWritting
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: new IconButton(
                    icon: new Icon(
                      Icons.attachment,
                      color: Colors.white,
                    ),
                    onPressed: () async {
                      var image = await ImagePicker.pickImage(
                          source: ImageSource.gallery);
                      bl = new BottomLoader(
                          context,
                          showLogs: false,
                          isDismissible: true,
                          loader: AwesomeLoader(loaderType: AwesomeLoader.AwesomeLoader2,
                            color: dark_blue,)
                        //loader: Image.asset('stopwatch.png', height: 10, width: 10,color: Colors.black,)
                      );
                      bl.style(
                          message: 'Please wait...',
                          messageTextStyle: TextStyle(
                              color: dark_blue,
                              fontSize: 12
                          )
                      );
                      //bl.display();
                      int timestamp = new DateTime.now().millisecondsSinceEpoch;
                      firebase_storage.Reference storageReference =
                          firebase_storage.FirebaseStorage.instance.ref().child(
                              'chats/img_' + timestamp.toString() + '.jpg');
                      firebase_storage.UploadTask uploadTask =
                          storageReference.putFile(image);
                      await uploadTask.snapshot.storage;


                      new Future.delayed(Duration(seconds: 15), () async {
                        if(uploadTask.snapshot.state == firebase_storage.TaskState.success) {
                          String fileUrl = await storageReference
                              .getDownloadURL();
                          _sendImage(messageText: null, imageUrl: fileUrl);
                        }
                        //bl.close();
                      });

                    }),
              ),
              new Flexible(
                child: new TextField(
                  controller: _textController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isWritting = messageText.length > 0;
                    });
                  },
                  //onSubmitted: _sendText,
                  style: TextStyle(color: Colors.white),
                  decoration: new InputDecoration.collapsed(
                      hintText: "Send a message",
                      hintStyle: TextStyle(color: Colors.white)),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: getDefaultSendButton(),
              ),
            ],
          ),
        ));
  }

  Future<Null> _sendText(String text) async {
    //_addMessage(text);
    if (text != "") {
      _textController.clear();
      chatReference.add({
        'text': text,
        'sender_id': widget.prefs.getString('uid'),
        'sender_name': widget.prefs.getString('name'),
        'profile_photo': widget.prefs.getString('profile_photo'),
        'image_url': '',
        'time': FieldValue.serverTimestamp(),
      }).then((documentReference) {
        setState(() {
          _isWritting = false;
        });
      }).catchError((e) {});
    }
  }

  void _sendImage({String messageText, String imageUrl}) {
    //bl.close();
    chatReference.add({
      'text': messageText,
      'sender_id': widget.prefs.getString('uid'),
      'sender_name': widget.prefs.getString('name'),
      'profile_photo': widget.prefs.getString('profile_photo'),
      'image_url': imageUrl,
      'time': FieldValue.serverTimestamp(),
    });
  }

  Future<void> _addMessage(String message) async {
    //_textController.clear();

    /*_textMessages.add(isSelfMode
        ? TextMessage.createForLocalUser(
            message, DateTime.now().millisecondsSinceEpoch)
        : TextMessage.createForRemoteUser(
            message, DateTime.now().millisecondsSinceEpoch));*/

    _textMessages.add(TextMessage.createForLocalUser(
        message, DateTime.now().millisecondsSinceEpoch));

    await updateSmartReplies();

    if (!mounted) return;

    //isSelfMode = !isSelfMode;

    setState(() {});
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _textController.dispose();
    super.dispose();
  }
}
