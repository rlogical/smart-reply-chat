import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_app/registration_page.dart';
import 'package:flutter_chat_app/utils/app_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences.getInstance().then((prefs) {
    runApp(LandingPage(prefs: prefs));
  });
}

class LandingPage extends StatelessWidget {
  final SharedPreferences prefs;
  LandingPage({this.prefs});

  @override
  Widget build(BuildContext context) {
    changeStatusBar(Brightness.dark);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: _decideMainPage(),
    );
  }

  _decideMainPage() {
    if (prefs.getBool('is_verified') != null) {
      if (prefs.getBool('is_verified')) {
        return HomePage(prefs: prefs);
      } else {
        return RegistrationPage(prefs: prefs);
      }
    } else {
      return RegistrationPage(prefs: prefs);
    }
  }
}